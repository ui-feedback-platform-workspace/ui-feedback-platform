from django.shortcuts import render
from django.http import JsonResponse
from django.views import View
from .models import Example                     # add this
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated


from rest_framework.views import APIView
from rest_framework.response import Response

class ExampleView(View):
    def get(self, request):
        headers = request.headers
        print(headers)
        return JsonResponse({
            'foo':'bar',
        })

class HelloView(APIView):
    # authentication_classes = [JWTAuthentication]
    # permission_classes = [IsAuthenticated]

    def get(self, request):
        return Response({"message":"Hello"})

    def post(self, request):
        return Response({"message":"Hello"})
