from django.contrib import admin

from .models import Example

class ExampleAdmin(admin.ModelAdmin):
  list_display = ('title', 'description', 'completed')

# Register your models here.
admin.site.register(Example, ExampleAdmin)