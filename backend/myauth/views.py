from django.shortcuts import render
from rest_framework.views import APIView
from .serializers import MyTokenObtainPairSerializer
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
)
from rest_framework.response import Response
from django.contrib.auth.models import User
from .serializers import *
import json
from rest_framework_simplejwt.authentication import JWTAuthentication
from .models import *
from .utils import *


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer

class RegisterView(APIView):
    def post(self, request):
        serializer = RegisterSerializer(data=request.data)

        if serializer.is_valid():
            data = serializer.data
            user = User.objects.create_user(
                email=data["email"],
                username=data["username"],
                password=data["password"],
                **{
                    "first_name": data["first_name"] if "first_name" in data else "",
                    "last_name": data["last_name"] if "last_name" in data else ""
                }
            )

            return Response(get_tokens_for_user(user))

        else:
            return Response({"error": [
                "Invalid data passed, mabye malformed username, email or password?"
            ]}, status=400)



    def get(self, request):
        return Response({ "message" : "lets register!" })

class IndexView(APIView):
    def get(self, request):
        return Response({"message":"hi"})