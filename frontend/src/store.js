import { configureStore, combineReducers, getDefaultMiddleware } from '@reduxjs/toolkit';

import authReducer from './slices/authSlice';
import alertsReducer from './slices/alertsSlice';

const combinedReducer = combineReducers({
    auth: authReducer,
    alerts: alertsReducer,
});

const rootReducer = (state, action) => {
    if (action.type === 'clearState') {
        return undefined;
    }
    return combinedReducer(state, action);
};

const store = configureStore({
    reducer: rootReducer,
    middleware: [...getDefaultMiddleware()],
});

export default store;
