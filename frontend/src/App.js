import React from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import Landing from './components/layout/Landing';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import MyNavbar from './components/layout/MyNavbar';
import store from './store';
import AlertContainer from './components/layout/AlertContainer';
import PrivateRoute from './components/routing/PrivateRoute';
import Dashboard from './components/dashboard/Dashboard';
import Post from './components/post/Post';
import Feed from './components/feed/Feed';

function App() {
    return (
        <Provider store={store}>
            <Router>
                <MyNavbar />
                <AlertContainer />
                <Switch>
                    <Route exact path="/">
                        <Landing />
                    </Route>
                    <Route path="/register">
                        <Register />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    {/* Private Routes */}
                    <PrivateRoute exact path="/dashboard" component={Dashboard} />
                    <PrivateRoute exact path="/post" component={Post} />
                    <PrivateRoute exact path="/feed" component={Feed} />
                </Switch>
            </Router>
        </Provider>
    );
}

export default App;
