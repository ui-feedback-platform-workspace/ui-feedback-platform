/* eslint-disable indent */
/** eslint-disable */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { selectAll, isAuthenticated } from '../../slices/authSlice';

/* eslint-disable react/jsx-props-no-spreading */
const PrivateRoute = ({ component: Component, ...rest }) => {
    const isAuthenticatedUser = useSelector(isAuthenticated);
    const loading = useSelector(selectAll).status === 'loading';

    return (
        <Route
          {...rest}
            // eslint-disable-next-line react/jsx-indent-props
            render={(props) => (!isAuthenticatedUser && !loading
                ? (<Redirect to="/login" />)
                : (<Component {...props} />))}
        />
    );
};

PrivateRoute.propTypes = {
    component: PropTypes.elementType.isRequired,
};

export default PrivateRoute;
