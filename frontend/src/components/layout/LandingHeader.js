import React, { Fragment } from 'react';
import { Button } from 'react-bootstrap';
import LandingPageHeader from '../../assets/landing-page-header.png';
import RobotAnimation from '../../assets/robot-animation.svg';

const LandingHeader = () => (
    <>
        <img className="w-full relative text-center" alt="landing-page-header" src={LandingPageHeader} />
        <div className="text-over-image">
            <h1 className="font-5xl">
                Get Real User
                <br />
                Feedback
                {' '}
                <br />
                Quick,
            </h1>
            <subtitle className="font-lg">
                Its that easy, post prototypes and
                <br />
                get instant user feedback quick,
                {' '}
                <br />
                simple, no hassle
            </subtitle>
            <br />
            <br />
            <Button variant="secondary">Get Started</Button>
        </div>
        <img className="w-4/5 centered" alt="robot-animation" src={RobotAnimation} />
    </>

);

export default LandingHeader;
