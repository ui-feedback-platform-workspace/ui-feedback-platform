import React from 'react';
import LandingHeader from './LandingHeader';

const Landing = () => (
    <>
        <LandingHeader />
    </>
);

export default Landing;
