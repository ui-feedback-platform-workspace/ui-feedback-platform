import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './layout.css';

const MyNavbar = () => (
    <Navbar bg="dark" variant="dark" sticky="top" id="navbar">
        <Container>
            <Navbar.Brand><Link to="/" className="default-font-color">Welcome</Link></Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Item><Link to="/" className="m-1 default-font-color">Home</Link></Nav.Item>
                <Nav.Item><Link to="/dashboard" className="m-1 default-font-color">Dashboard</Link></Nav.Item>
                <Nav.Item><Link to="/post" className="m-1 default-font-color">Post</Link></Nav.Item>
                <Nav.Item><Link to="/feed" className="m-1 default-font-color">Feed</Link></Nav.Item>
                <Nav.Item><Link to="/#" className="m-1 default-font-color">Features</Link></Nav.Item>
                <Nav.Item><Link to="/login" className="m-1 default-font-color">Login</Link></Nav.Item>
                <Nav.Item><Link to="/register" className="m-1 default-font-color">Register</Link></Nav.Item>
            </Nav>
        </Container>
    </Navbar>
);

export default MyNavbar;
