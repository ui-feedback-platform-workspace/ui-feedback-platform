import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Alert } from 'react-bootstrap/';
import { removeAlert } from '../../slices/alertsSlice';

const AlertContainer = () => {
    const dispatch = useDispatch();
    const alerts = useSelector((state) => state.alerts);

    const removeAlertHandler = (event) => {
        dispatch(removeAlert(event.target.getAttribute('message')));
    };
    return (
        <>
            <div id="alerts" className="absolute z-10 w-screen">
                {alerts.map(({ message, type }) => (
                    <Alert className="m-0" key={message} variant={type}>
                        {message}
                        <Button message={message} variant="light" className="m-2" onClick={removeAlertHandler}>&#10005;</Button>
                    </Alert>
                ))}
            </div>
        </>
    );
};

export default AlertContainer;
