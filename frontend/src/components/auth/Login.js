/** eslint-disable */
import React, { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import './auth.css';
import { useDispatch, useSelector } from 'react-redux';
import Form from 'react-bootstrap/Form';
import { login, isAuthenticated } from '../../slices/authSlice';

const Login = () => {
    const [email, setEmail] = useState(() => '');
    const [password, setPassword] = useState(() => '');
    const dispatch = useDispatch();
    const isAuthenticatedUser = useSelector(isAuthenticated);

    const handleSubmit = async (event) => {
        event.preventDefault();
        await dispatch(login({ email, password }));
    };

    if (isAuthenticatedUser) {
        return <Redirect to="/" />;
    }

    return (
        <div id="login" className="container-10">
            <Card className="w-1/4 mx-auto shadow-5 p-10  mt-12">
                <Card.Body>
                    <Card.Title className="text-center"><span className="font-bold font-lg">Login</span></Card.Title>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} />
                            <Form.Text className="text-muted">
                                We&apos;ll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Remember me" />
                        </Form.Group>

                        <Button variant="primary" type="submit" value="submit">
                            Login
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </div>
    );
};

export default Login;
