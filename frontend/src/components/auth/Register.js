import React, { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { register } from '../../slices/authSlice';

const Register = () => {
    const [email, setEmail] = useState(() => '');
    const [username, setUsername] = useState(() => '');
    const [firstName, setFirstName] = useState(() => '');
    const [lastName, setLastName] = useState(() => '');
    const [password, setPassword] = useState(() => '');
    const [repeatPassword, setRepeatPassword] = useState(() => '');
    const dispatch = useDispatch();
    const history = useHistory();

    const handleSubmit = async (event) => {
        event.preventDefault();
        const registerResult = await dispatch(register({
            email, username, firstName, lastName, password, repeatPassword,
        }));
        if (registerResult.meta.requestStatus === 'fulfilled') {
            history.push('/');
        }
    };

    return (
        <div id="login" className="container-10">
            <Card className="w-1/4 mx-auto shadow-5 p-10  mt-12">
                <Card.Body>
                    <Card.Title className="text-center"><span className="font-bold font-lg">Register</span></Card.Title>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Enter Username" value={username} onChange={(e) => setUsername(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter email" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter email" value={lastName} onChange={(e) => setLastName(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Repeat Password</Form.Label>
                            <Form.Control type="password" placeholder="Repeat password" value={repeatPassword} onChange={(e) => setRepeatPassword(e.target.value)} />
                        </Form.Group>

                        <Button className="mt-2" variant="primary" type="submit" value="submit">
                            Login
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </div>
    );
};

export default Register;
