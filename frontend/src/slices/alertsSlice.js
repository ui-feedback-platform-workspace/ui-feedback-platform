import {
    createSlice,
} from '@reduxjs/toolkit';

const initialState = [
    // {message: message, type: danger | warning | info | success }
];

const alertsSlice = createSlice({
    name: 'alerts',
    initialState,
    reducers: {
        addAlert: (state, action) => {
            state.push(action.payload);
        },
        removeAlert: (state, action) => state.filter((alert) => alert.message !== action.payload),
    },
});

export const {
    addAlert,
    removeAlert,
} = alertsSlice.actions;

export default alertsSlice.reducer;
export const selectAll = (state) => state.alert;
