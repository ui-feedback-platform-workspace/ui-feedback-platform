import {
    createSlice,
    createAsyncThunk,
} from '@reduxjs/toolkit';
import _ from 'lodash';
import axios from 'axios';
import { addAlert } from './alertsSlice';

export const login = createAsyncThunk(
    'auth/login',
    async ({ email, password }, { rejectWithValue, dispatch }) => {
        try {
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await axios.post('/api/auth/token/', {
                email,
                password,
            }, config);

            dispatch(addAlert({
                message: 'Successful logged in',
                type: 'success',
            }));

            return {
                refreshToken: response.data.refresh,
                accessToken: response.data.access,
                email: 'sample@gmail.com',
                username: 'someUsername',
            };
        } catch (err) {
            dispatch(addAlert({
                message: 'Invalid credentials, error logging in',
                type: 'danger',
            }));
            return rejectWithValue();
        }
    },
);

export const register = createAsyncThunk(
    'auth/register',
    async ({
        email, username, firstName, lastName, password, repeatPassword,
    }, { rejectWithValue, dispatch }) => {
        try {
            if (password !== repeatPassword) {
                dispatch(addAlert(
                    'passwords do not match',
                    'danger',
                ));
                return rejectWithValue();
            }

            const config = {
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await axios.post('/api/auth/register/', {
                email,
                username,
                password,
                first_name: firstName,
                last_name: lastName,
            }, config);

            dispatch(addAlert({
                message: `Successful registered user: ${username}`,
                type: 'success',
            }));

            return {
                refreshToken: response.data.refresh,
                accessToken: response.data.access,
                email: 'sample@gmail.com',
                username: 'someUsername',
            };
        } catch (err) {
            dispatch(addAlert({
                message: 'User already exists, error registering user',
                type: 'danger',
            }));
            return rejectWithValue();
        }
    },
);

const initialState = {
    status: 'idle', // or: 'loading', 'succeeded', 'failed'
    authInfo: {
        // user_id: null,
        // email: null,
        // username: null,
    },
};

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
    },
    extraReducers: {
        [login.pending]: (state, action) => ({
            ...state,
            status: 'loading',
        }),
        [login.fulfilled]: (state, action) => ({
            ...state,
            status: 'fulfilled',
            authInfo: action.payload,
        }),
        [login.rejected]: (state, action) => ({
            ...state,
            status: 'failed',
            auth_info: [],
        }),
        [register.pending]: (state, action) => ({
            ...state,
            status: 'loading',
        }),
        [register.fulfilled]: (state, action) => ({
            ...state,
            status: 'fulfilled',
            authInfo: action.payload,
        }),
        [register.rejected]: (state, action) => ({
            ...state,
            status: 'failed',
            auth_info: [],
        }),
    },
});

export default authSlice.reducer;
export const selectAll = (state) => state.auth;
export const isAuthenticated = (state) => !_.isEmpty(state ? state.auth.authInfo : null);
